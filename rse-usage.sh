#!/bin/sh

export X509_USER_PROXY=/data/ddmusr01/ddmusr01_latest_x509up.rfc.proxy
export RUCIO_ACCOUNT=ddmadmin
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q
lsetup rucio -q

source /data/ddmusr01/storage_monitor/rrdvenv/bin/activate

python3 /data/ddmusr01/storage_monitor/rse-usage.py
