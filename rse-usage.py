#!/usr/bin/env python3
#
# Print and plot the RSE limits and usage
#
# Tested with python 3.6 and 3.8
#
from datetime import datetime
import json
import logging
import os
import re
import requests
import rrdtool
import sys
import traceback
from types import MethodType
from urllib.parse import urlparse
from rucio.client import Client
from rucio.common.exception import RucioException, RSENotFound, InvalidRSEExpression
import socket
try:
    from influxdb import InfluxDBClient as idb
except:
    idb = None

class Wrapper:
    '''
    Wrapper to Rucio calls. Catches exceptions and handles retries in a
    rather simple way, just repeating the call up to 3 times.
    '''
    def __init__(self, other, name):
        self.other = other
        self.logger = logging.getLogger(name)

    def __getattr__(self, name):
        if hasattr(self.other, name):
            func = getattr(self.other, name)
            return lambda *args, **kwargs: self._wrap(func, args, kwargs)
        raise AttributeError(name)

    def _wrap(self, func, args, kwargs):
        result = None
        for attempt in range(3):
            try:
                if type(func) == MethodType:
                    result = func( *args, **kwargs)
                else:
                    result = func(self.other, *args, **kwargs)
            except RucioException as e:
                self.logger.error("Rucio exception: %s (attempt %i)", str(e), attempt+1)
            except Exception as e:
                self.logger.error("Unexpected exception: %s (attempt %i)", str(e), attempt+1)
            else:
                break
        return result

class StorageMonitor:
    '''Collect usage info on RSEs and make tables and rrd plots'''

    def __init__(self):
        self.wwwdir='/var/www/html/ddmusr01/'
        self.plotsdir='%s/plots' % self.wwwdir
        try:
            os.makedirs(self.plotsdir)
        except OSError:
            pass

        # Put manually excluded RSEs in this list
        self.exclusions = []
        self.primratio = 0.6
        if idb:
            try:
                from ConfigParser import ConfigParser # py2
            except ImportError:
                from configparser import ConfigParser # py3
            cp = ConfigParser()
            cp.readfp(open('/etc/adc_influxdb.conf'))
            self.influx_client = idb(cp.get('config','host'), int(cp.get('config','port')), cp.get('config','user'), cp.get('config','pass'), cp.get('config','db'), ssl=True, verify_ssl=False)

        # {RSE: {"Total(storage)": 1234, "Used(rucio)": 1000}, ... }
        self.jsondata = {}

        # TODO: Get these numbers automatically from CRIC
        # Disk pledges
        self.diskpledges = {2016: 138*10**15,
                            2017: 172*10**15,
                            2018: 193*10**15,
                            2019: 221*10**15,
                            2020: 234*10**15,
                            2021: 272*10**15,
                            2022: 304*10**15,
                            2023: 350*10**15,
                            2024: 412*10**15}

        # Tape pledges
        self.tapepledges = {2016: 161*10**15,
                            2017: 252*10**15,
                            2018: 302*10**15,
                            2019: 311*10**15,
                            2020: 319*10**15,
                            2021: 336*10**15,
                            2022: 400*10**15,
                            2023: 534*10**15,
                            2024: 659*10**15}

        # All pledges
        self.allpledges = {2016: 299*10**15,
                           2017: 424*10**15,
                           2018: 494*10**15,
                           2019: 532*10**15,
                           2020: 553*10**15,
                           2021: 608*10**15,
                           2022: 704*10**15,
                           2023: 884*10**15,
                           2024: 1071*10**15}

        if os.path.exists('/etc/ssl/certs/CERN-bundle.pem'):
            self.ca_cert = '/etc/ssl/certs/CERN-bundle.pem'
        else:
            self.ca_cert = False
        self.rucio = Wrapper(Client(ca_cert=self.ca_cert), 'RucioWrapper')
        self.certificate = os.environ.get('X509_USER_PROXY', '/tmp/x509up_u{}'.format(os.getuid()))

        self.setuplogging()
        if not self.certificate or not os.path.isfile(self.certificate):
            self.logger.error("X509_USER_PROXY must be defined and point to a valid certificate")
            sys.exit(1)
        self.collectdata()


    def setuplogging(self):
        '''Set up logging to stdout'''
        # make this logger the root logger, taking over from Rucio's logger
        self.logger = logging.getLogger()
        self.logger.handlers = []

        try:
            fmt = logging.Formatter(fmt="%(asctime)s %(levelname)s %(message)s")
            handler = logging.StreamHandler(sys.stdout)
            handler.setFormatter(fmt)
            self.logger.setLevel(logging.DEBUG)
            self.logger.addHandler(handler)
            self.logger.propagate = False # to stop also sending to stderr
        except Exception as e:
            self.logger.error("Failed to set up logging: %s - will log to stderr at WARNING level", str(e))
            self.logger.setLevel(logging.WARNING)


    def collectdata(self):
        '''Collect info from external sources'''
        try:
            # Get all the DDM endpoints from CRIC
            self.data = requests.get('https://atlas-cric.cern.ch/api/atlas/ddmendpoint/query/?json&state=ACTIVE&site_state=ACTIVE',
                                      cert=self.certificate,
                                      verify=self.ca_cert).json()
            # add hostname of storage and quotas
            for name, attr in list(self.data.items()):
                host = urlparse(attr.get('se') or '').hostname
                if host:
                    self.data[name]['se_hostname'] = host
                self.data[name]['quotas_sum'] = sum(attr['quotas'].values())
        except Exception as e:
            self.logger.error("Failed to get information from CRIC: {0}".format(str(e)))
            sys.exit(1)

        try:
            # Get tombstone info from current.json
            tsdata = requests.get('https://rucio-hadoop.cern.ch/dumps/rse_usage/current.json', verify=self.ca_cert).json()
            for r, attrs in list(tsdata.items()):
                if 'avg(now-tombstone) in days' in attrs and r in self.data:
                    self.data[r]['avg_tombstone'] = attrs['avg(now-tombstone) in days']
        except Exception as e:
            self.logger.error("Failed to get tombstones info: {0}".format(str(e)))


    def TB(self, size):
        '''Convert size to TB'''
        if size == 'n/a':
            return 'n/a'
        return '%.2f' % (size/1000.0**4)


    def addpledge(self, args, rrdfile, pledges):
        '''Make pledge lines on the rrd plots'''

        # 1 April timestamps
        pledgedates = dict((i,datetime(i, 4, 1).strftime('%s')) for i in range(2016, 2026))

        # Add dummy CDEF needed for pledge CDEF
        args.append('CDEF:dummy=srmtotal,0,MIN')
        legend = True
        for year, pledge in pledges.items():
            args.append('CDEF:pledge%d=TIME,%s,GT,TIME,%s,LE,*,%d,dummy,-,UNKN,IF' % (year, pledgedates[year], pledgedates[year+1], pledges[year]))
            args.append('LINE2:pledge%d#000000%s' % (year, ':Pledge' if legend else ''))
            legend = False


    def makeplot(self, name, storagetotal, rucioused, usedothers, quotaothers, dark, storagefree, temporary, unlocked, limit, difference):
        '''Add info to the rrd for the given RSE'''
        # Note: srm is still used in the rra names even though the info is protocol-independent 
        # Check if rrd file exists
        try:
            fields = {
                'srmtotal':storagetotal*1.0, 'rucioused':rucioused*1.0, 'usedothers':usedothers*1.0, 
                'quotaothers':quotaothers*1.0, 'dark':dark*1.0, 'srmfree': storagefree*1.0, 'temporary': temporary*1.0,
                'unlocked':unlocked*1.0, 'limit':limit*1.0, 'difference':difference*1.0
            }
            json_body = [ { "measurement": "site_occupancy", "tags": { "site": name }, "fields": fields } ]
            self.influx_client.write_points(json_body)
        except Exception as e:
            if idb:
                self.logger.error('Sending data to the InfluxDB failed for {0}: {1}'.format(name, str(e)))

        rrdfile = '%s/%s.rrd' % (self.plotsdir, name)
        if not os.path.exists(rrdfile):
            self.logger.info('Creating rrd file for {0}'.format(name))
            try:
                rrdtool.create(str(rrdfile), '--step', '3600',
                               'DS:srmtotal:GAUGE:36000:0:100000000000000000000',
                               'DS:rucioused:GAUGE:36000:0:100000000000000000000',
                               'DS:usedothers:GAUGE:36000:0:100000000000000000000',
                               'DS:quotaothers:GAUGE:36000:0:100000000000000000000',
                               'DS:dark:GAUGE:36000:U:100000000000000000000',
                               'DS:srmfree:GAUGE:36000:U:100000000000000000000',
                               'DS:unlocked:GAUGE:36000:0:100000000000000000000',
                               'DS:limit:GAUGE:36000:U:100000000000000000000',
                               'DS:difference:GAUGE:36000:U:100000000000000000000',
                               'DS:temporary:GAUGE:36000:U:100000000000000000000',
                               'RRA:AVERAGE:0.5:1:87600')
            except Exception as e:
                self.logger.error('Could not create rrd archive: {0}'.format(str(e)))
                return
        try:
            rrdtool.update(str(rrdfile), 'N:%i:%i:%i:%i:%i:%i:%i:%i:%i:%i' % (storagetotal, rucioused, usedothers, quotaothers, dark, storagefree, unlocked, limit, difference, temporary))
            for timerange in ['1d', '1w', '1m', '1y', 'all']:
                rrdplot = '%s/%s-%s.png' % (self.plotsdir, name, timerange)
                args = [str(rrdplot), # output filename
                        '--start', '%s' % ('now-%s' % str(timerange) if timerange != 'all' else '1459468800'), # Start time (1459468800 is 1 April 2016)
                        '--lower-limit', '0', # Start Y-axis from zero
                        '--width', '%s' % ('600' if timerange != 'all' else '1200'), # Image width
                        '--height', '300', # Image height
                        '--title', '%s - %s' % (str(name), str(timerange)), # Title
                        '--vertical-label', 'bytes', # Label Y-axis
                        #'--left-axis-format', '%sB', # This requires a newer version of rrdtool
                        '--font', 'DEFAULT:10:Nimbus Sans L', '--font', 'WATERMARK:8:Nimbus Sans L', # Nicer fonts
                       ]
                if timerange == 'all':
                    args.extend(['-x', 'MONTH:1:YEAR:1:YEAR:1:31536000:%Y']) # Force year labels
                args.extend([ # Archives and combined values to plot
                          'DEF:srmtotal=%s:srmtotal:AVERAGE' % str(rrdfile),
                          'DEF:rucioused=%s:rucioused:AVERAGE' % str(rrdfile),
                          'DEF:usedothers=%s:usedothers:AVERAGE' % str(rrdfile),
                          'DEF:quotaothers=%s:quotaothers:AVERAGE' % str(rrdfile),
                          'DEF:dark=%s:dark:AVERAGE' % str(rrdfile),
                          'DEF:srmfree=%s:srmfree:AVERAGE' % str(rrdfile),
                          'DEF:temporary=%s:temporary:AVERAGE' % str(rrdfile),
                          'DEF:unlocked=%s:unlocked:AVERAGE' % str(rrdfile),
                          'DEF:limit=%s:limit:AVERAGE' % str(rrdfile),
                          'CDEF:persistent=rucioused,unlocked,-,temporary,UN,0,temporary,IF,-,0,MAX',
                          'CDEF:maxused=srmtotal,limit,-,0,MAX',
                          'CDEF:darkpos=0,dark,MAX',
                          # Draw the areas and lines
                          'AREA:usedothers#0066FF:Group',
                          'AREA:persistent#FFCC00:Persistent:STACK',
                          'AREA:temporary#FFA500:Temporary:STACK',
                          'AREA:unlocked#009900:Cache:STACK',
                          'AREA:darkpos#CCCCCC:Dark:STACK',
                          'LINE2:srmtotal#FF3300:Storage total',
                          'LINE2:quotaothers#0033FF:Group quota:dashes',
                          'LINE2:maxused#999999:Space limit:dashes'
                          ])
                if name == 'TOTAL_ALL_PLEDGED_DISK':
                    self.addpledge(args, rrdfile, self.diskpledges)
                elif name == 'TOTAL_ALL_PLEDGED_TAPE':
                    self.addpledge(args, rrdfile, self.tapepledges)
                elif name == 'TOTAL_ALL_PLEDGED':
                    self.addpledge(args, rrdfile, self.allpledges)
                rrdtool.graph(*args)
        except Exception as e:
            self.logger.error('Error creating rrd graph for {0}: {1}'.format(name, str(e)))


    def maketable(self, expression, tablename, defaultlimit=0.0, plots=True, totalplots=True):
        '''Make html tables for the RSEs in expression'''

        html = '<html>\n'
        html += '<head><script src="sorttable.js"></script>\n'
        html += '<style>\n'
        html += '.datapolicyt0disk_false { color: #444444; font-style: italic;}\n'
        html += '</style>\n'
        html += '</head><body>\n'

        html += '<table style="width:1420" class="sortable">\n'
        html += '<h2>%s</h2>\n' % tablename
        html += '<tr><th style="width:300px">RSE</th>\n'
        html += '<th style="width:100px">Total (storage)</th>\n'
        html += '<th style="width:100px">Used (rucio)</th>\n'
        html += '<th style="width:100px">Used (other)</th>\n'
        html += '<th style="width:100px">Quota (other)</th>\n'
        html += '<th style="width:100px">Used (dark)</th>\n'
        html += '<th style="width:100px">Used (persistent)</th>\n'
        html += '<th style="width:100px">Used (temporary)</th>\n'
        html += '<th style="width:100px">Unlocked (secondary)</th>\n'
        html += '<th style="width:100px">Free (storage)</th>\n'
        html += '<th style="width:100px">Min space</th>\n'
        html += '<th style="width:100px">Difference</th>\n'
        html += '<th style="width:100px">Primary diff</th>\n'
        html += '<th style="width:120px">Avg tombstone</th></tr>\n'

        tstoragetotal = trucioused = tusedothers = tdark = tpersistent = ttemporary = tunlocked = tstoragefree = tlimit = tdifference = tquotaothers = tprimdiff = ttombstones = 0
        try:
            rses = [r['rse'] for r in self.rucio.list_rses(expression)]
        except InvalidRSEExpression: # No RSEs matched the expression
            rses = []
        rses.sort()
        groupdisks = {}
        for name in rses:
            if name in self.exclusions:
                continue

            if name not in self.data:
                self.logger.warning('{0} not in active CRIC endpoints, skipping'.format(name))
                continue
    
            attrs = self.data[name]
            css_class = 'datapolicyt0disk_false'
            try:
                self.logger.info(name)
                rseinfo = dict((r['source'], r) for r in self.rucio.get_rse_usage(name))
                # 'storage' and 'rucio' fields must be present
                if 'storage' not in rseinfo:
                    self.logger.warning('{0}: missing storage information'.format(name))
                    storagetotal = 0
                    storagefree = 0
                    storageused = -1
                    storagetimestamp = datetime.utcnow()
                else:
                    storagetotal = rseinfo.get('storage')['total']
                    storagefree = rseinfo['storage']['free']
                    storageused = rseinfo['storage']['used']
                    storagetimestamp = rseinfo['storage']['updated_at']
                rucioused = rseinfo['rucio']['used']
                # If 'expired' is not present it means no expired
                unlocked = rseinfo.get('expired', {}).get('used', 0)
                # If 'unavailable' is not present it means no unavailable
                unavailable = rseinfo.get('unavailable', {}).get('used', 0)
                # If 'persistent' is not present it means no persistent
                persistent = rseinfo.get('persistent', {}).get('used', 0)
                # If 'temporary' is not present it means no temporary
                temporary = rseinfo.get('temporary', {}).get('used', 0)
                # Min space limit is only set for centrally managed RSEs
                limit = max(rseinfo.get('min_free_space', {}).get('used', defaultlimit), 0)

                rucioused -= unavailable 
                if storageused == -1: # Non-SRM sites can report -1 for used space
                    storageused = rucioused
                    storagefree = storagetotal - rucioused
                if 'T0Disk' in attrs.get('datapolicies', []):
                    css_class = ''

                # Get space on RSEs sharing same token
                usedothers = 0
                quotaothers = 0

                if attrs['type'] == 'GROUPTAPE':
                    usedothers = rucioused
                    quotaothers = attrs['quotas_sum']*(1000**4)
                    rucioused = 0
                # For CERN ignore group since it messes up tape numbers
                elif attrs.get('site') != 'CERN-PROD':
                    for endpoint, endpoint_attrs in self.data.items():
                        try:
                            if endpoint != name and \
                               endpoint_attrs['se_hostname'] == attrs['se_hostname'] and \
                               endpoint_attrs['token'] == attrs['token'] and \
                               endpoint_attrs['type'] in ('GROUPDISK', 'GROUPTAPE'):
                                try:
                                    usedothers += [r['used'] for r in self.rucio.get_rse_usage(endpoint, filters={'source': 'rucio'})][0]
                                    quotaothers += endpoint_attrs['quotas_sum']*(1000**4)
                                except RSENotFound:
                                    pass
                        except:
                            pass

                # Collect info on group disks outside of datadisk and add it later
                if attrs['type'] == 'GROUPDISK' and attrs['token'] != 'ATLASDATADISK':
                    site = attrs['site'].upper()
                    if site not in groupdisks:
                        groupdisks[site] = []
                    groupdisks[site].append({'storagetotal': storagetotal,
                                             'storageused': storageused,
                                             'storagetimestamp': storagetimestamp,
                                             'rucioused': rucioused,
                                             'persistent': persistent,
                                             'temporary': temporary,
                                             'unlocked': unlocked,
                                             'quota': attrs['quotas_sum']*(1000**4)})
                    continue

                dark = storageused - rucioused - usedothers
                difference = storagefree - limit
                primdiff = (rucioused-unlocked) - (storagetotal-quotaothers)*self.primratio
                try:
                    tombstoneavg = '%.2f' % float(attrs['avg_tombstone'])
                except:
                    tombstoneavg = 'n/a'
                if plots:
                    self.makeplot(name, storagetotal, rucioused, usedothers, quotaothers, dark, storagefree, temporary, unlocked, limit, difference)

            except Exception as e:
                self.logger.error('{0}: missing information: {1}'.format(name, str(e)))
                storagetotal = storagefree = storageused = rucioused = unlocked = dark = persistent = temporary = limit = difference = usedothers = quotaothers = primdiff = tombstoneavg = 'n/a'

            if difference == 'n/a':
                color = 'black'
            elif float(difference) < -(1000**4):
                color = 'red'
            else:
                color = 'green'

            plotlink = '<a href="plots/plots.php?endpoint=%s">%s</a>' % (name, name)
            self.jsondata[name] = {}
            html += '<tr class="%s"><td>%s</td>' % (css_class, plotlink)
            html += '<td align="right">%s</td>' % self.TB(storagetotal); self.jsondata[name]['Total(storage)'] = self.TB(storagetotal)
            html += '<td align="right">%s</td>' % self.TB(rucioused);    self.jsondata[name]['Used(rucio)'] = self.TB(rucioused)
            html += '<td align="right">%s</td>' % self.TB(usedothers);   self.jsondata[name]['Used(other)'] = self.TB(usedothers)
            html += '<td align="right">%s</td>' % self.TB(quotaothers);  self.jsondata[name]['Quota(other)'] = self.TB(quotaothers)
            html += '<td align="right">%s</td>' % self.TB(dark);         self.jsondata[name]['Used(dark)'] = self.TB(dark)
            html += '<td align="right">%s</td>' % self.TB(persistent);   self.jsondata[name]['Persistent'] = self.TB(persistent)
            html += '<td align="right">%s</td>' % self.TB(temporary);    self.jsondata[name]['Temporary'] = self.TB(temporary)
            html += '<td align="right">%s</td>' % self.TB(unlocked);     self.jsondata[name]['Unlocked'] = self.TB(unlocked)
            html += '<td align="right">%s</td>' % self.TB(storagefree);  self.jsondata[name]['Free(storage)'] = self.TB(storagefree)
            html += '<td align="right">%s</td>' % self.TB(limit);
            if 'Min space' not in self.jsondata[name]: # To stop tape limits being reset
                self.jsondata[name]['Min space'] = self.TB(limit)
            html += '<td align="right"><font color="%s">%s</font></td>\n' % (color, self.TB(difference));   self.jsondata[name]['Difference'] = self.TB(difference)
            html += '<td align="right">%s</td>' % self.TB(primdiff);   self.jsondata[name]['Primary diff'] = self.TB(primdiff)
            html += '<td align="right">%s</td></tr>' % tombstoneavg;   self.jsondata[name]['Avg tombstone'] = tombstoneavg
            try:
                self.jsondata[name]['Storage Timestamp'] = storagetimestamp.isoformat()
            except:
                self.jsondata[name]['Storage Timestamp'] = datetime.fromtimestamp(0).isoformat()

            if storagetotal != 'n/a': tstoragetotal += storagetotal
            if rucioused    != 'n/a': trucioused += rucioused
            if usedothers   != 'n/a': tusedothers += usedothers
            if quotaothers  != 'n/a': tquotaothers += quotaothers
            if dark         != 'n/a': tdark += dark
            if persistent   != 'n/a': tpersistent += persistent
            if temporary    != 'n/a': ttemporary += temporary
            if unlocked     != 'n/a': tunlocked += unlocked
            if storagefree  != 'n/a': tstoragefree += storagefree
            if limit        != 'n/a': tlimit += limit
            if difference   != 'n/a': tdifference += difference
            if difference   != 'n/a': tprimdiff += primdiff

        # Add groupdisks separate from datadisk
        if groupdisks:
            css_class = 'datapolicyt0disk_false'
            for site, usage in groupdisks.items():
                name = '%s_GROUPDISK' % site
                self.logger.info(name)
                rucioused = limit = primdiff = tombstoneavg = 0
                storagetotal = usage[0]['storagetotal']
                usedothers = sum([u['persistent'] for u in usage])
                quotaothers = sum([u['quota'] for u in usage])
                persistent = sum([u['persistent'] for u in usage])
                temporary = sum([u['temporary'] for u in usage])
                unlocked = sum([u['unlocked'] for u in usage])
                storagefree = storagetotal - usage[0]['storageused']
                dark = storagetotal - storagefree - persistent - temporary - unlocked
                difference = storagetotal - storagefree
                plotlink = '<a href="plots/plots.php?endpoint=%s">%s</a>' % (name, name)
                self.jsondata[name] = {}
                html += '<tr class="%s"><td>%s</td>' % (css_class, plotlink)
                html += '<td align="right">%s</td>' % self.TB(storagetotal);  self.jsondata[name]['Total(storage)'] = self.TB(storagetotal)
                html += '<td align="right">%s</td>' % self.TB(rucioused);     self.jsondata[name]['Used(rucio)'] = self.TB(rucioused)
                html += '<td align="right">%s</td>' % self.TB(usedothers);    self.jsondata[name]['Used(other)'] = self.TB(usedothers)
                html += '<td align="right">%s</td>' % self.TB(quotaothers);   self.jsondata[name]['Quota(other)'] = self.TB(quotaothers)
                html += '<td align="right">%s</td>' % self.TB(dark);          self.jsondata[name]['Used(dark)'] = self.TB(dark)
                html += '<td align="right">%s</td>' % self.TB(persistent);    self.jsondata[name]['Persistent'] = self.TB(persistent)
                html += '<td align="right">%s</td>' % self.TB(temporary);     self.jsondata[name]['Temporary'] = self.TB(temporary)
                html += '<td align="right">%s</td>' % self.TB(unlocked);      self.jsondata[name]['Unlocked'] = self.TB(unlocked)
                html += '<td align="right">%s</td>' % self.TB(storagefree);   self.jsondata[name]['Free(storage)'] = self.TB(storagefree)
                html += '<td align="right">%s</td>' % self.TB(limit);         self.jsondata[name]['Min space'] = self.TB(limit)
                html += '<td align="right"><font color="%s">%s</font></td>\n' % (color, self.TB(difference));   self.jsondata[name]['Difference'] = self.TB(difference)
                html += '<td align="right">%s</td>' % self.TB(primdiff);      self.jsondata[name]['Primary diff'] = self.TB(primdiff)
                html += '<td align="right">%s</td></tr>' % tombstoneavg; self.jsondata[name]['Avg tombstone'] = tombstoneavg
                self.jsondata[name]['Storage Timestamp'] = storagetimestamp.isoformat()

                tstoragetotal += storagetotal
                tusedothers += usedothers
                tquotaothers += quotaothers
                tdark += dark
                tpersistent += persistent
                ttemporary += temporary
                tunlocked += unlocked
                tstoragefree += storagefree
                tdifference += difference

                self.makeplot(name, storagetotal, rucioused, usedothers, quotaothers, dark, storagefree, temporary, unlocked, limit, difference)

        if tdifference == 'n/a':
            color = 'black'
        elif float(tdifference) < 0.0:
            color = 'red'
        else:
            color = 'green'

        plotslink = 'TOTAL'
        if totalplots:
            self.makeplot('TOTAL_%s' % tablename, tstoragetotal, trucioused, tusedothers, tquotaothers, tdark, tstoragefree, ttemporary, tunlocked, tlimit, tdifference)
            plotslink = '<a href="plots/plots.php?endpoint=TOTAL_%s">TOTAL</a>' % tablename

        html += '</table>\n'
        html += '<table style="width:1300">\n'
        html += '<tr><td style="width:300px"><b>%s</b></td>' % plotslink
        html += '<td align="right" style="width:100px"><b>%s</b></td>' % self.TB(tstoragetotal)
        html += '<td align="right" style="width:100px"><b>%s</b></td>' % self.TB(trucioused)
        html += '<td align="right" style="width:100px"><b>%s</b></td>' % self.TB(tusedothers)
        html += '<td align="right" style="width:100px"><b>%s</b></td>' % self.TB(tquotaothers)
        html += '<td align="right" style="width:100px"><b>%s</b></td>' % self.TB(tdark)
        html += '<td align="right" style="width:100px"><b>%s</b></td>' % self.TB(tpersistent)
        html += '<td align="right" style="width:100px"><b>%s</b></td>' % self.TB(ttemporary)
        html += '<td align="right" style="width:100px"><b>%s</b></td>' % self.TB(tunlocked)
        html += '<td align="right" style="width:100px"><b>%s</b></td>' % self.TB(tstoragefree)
        html += '<td align="right" style="width:100px"><b>%s</b></td>' % self.TB(tlimit)
        html += '<td align="right" style="width:100px"><font color="%s"><b>%s</b></font></td>\n' % (color, self.TB(tdifference))
        html += '<td align="right" style="width:100px"><b>%s</b></td></tr>' % self.TB(tprimdiff)
        #html += '<td align="right" style="width:120px"><b></b></td></tr>'

        html += '</table><p>\n'

        html += '<p>\n'
        html += '<a href="index.html">Back to index</a><br>\n'
        html += 'This table was created using the RSE expression: <pre>%s</pre>\n' % expression
        html += 'All numbers in TB. Click on table headers to sort.<br>\n'
        html += 'Used (other) is other RSEs sharing the same space token.<br>\n'
        html += 'Quota (other) is the sum of quotas defined on other RSEs sharing the same space token.<br>\n'
        html += 'Used (dark) is the difference between Storage used and Rucio used for all the RSEs on the space token.<br>\n'
        html += 'Unlocked is data eligible for deletion.\n'
        html += '<p>Min space is a limit dynamically set by a collector based on storage capacity of the endpoint (Total(storage) - Used(other)) and according to the following policy:\n'
        # TODO: get these limits directly from Rucio
        html += '<ul><li>DATADISK: Min(10% or 300TB free)\n'
        html += '<li>SCRATCHDISK: Min(25% or 50TB free)\n'
        html += '<li>TAPE: 10TB (arbitrary limit for these tables - actual cleaning depends on the garbage collection algorithm on each site)\n'
        html += '</ul>\n'
        html += 'Primary diff - amount of primaries (UsedRucio-Unlocked) over a threshold: (Total(storage) - GroupQuota)*%f)<br>\n' % (self.primratio)
        html += 'Avg tombstone - days between now and timestamp of average tombstone, giving an indication of the turnover of secondary data\n'
        html += '<p>Generated at %s UTC on %s. This page is updated once per hour.</p>\n' % (str(datetime.utcnow()), socket.gethostname())
        html += '</body>\n</html>\n'
        with open('%s/%s.html' % (self.wwwdir, tablename), 'w') as f:
            f.write(html)


    def makeattributetable(self, expression, tablename):
        '''Make html tables with attributes for the RSEs in expression'''

        html = '<html>\n'
        html += '<head><script src="sorttable.js"></script>\n'
        html += '</head><body>\n'

        html += '<table style="width:1420" class="sortable">\n'
        html += '<h2>%s</h2>\n' % tablename
        html += '<tr><th style="width:300px">RSE</th>\n'

        attr_list = ['bb8-enabled',
                     'datapolicyanalysis',
                     'datapolicynucleus',
                     'datapolicyt0disk',
                     'datapolicyt0tape',
                     'datapolicyt0taskoutput',
                     'dontkeeplog',
                     'enablegreedy',
                     'todecommission']
        for attr in attr_list:
            html += '<th style="width:100px">{}</th>\n'.format(re.sub('datapolicy', 'datapolicy\n', attr))
        html += '</tr>\n'

        try:
            rses = [r['rse'] for r in self.rucio.list_rses(expression)]
        except InvalidRSEExpression: # No RSEs matched the expression
            rses = []
        rses.sort()

        for name in rses:
            if name in self.exclusions:
                continue

            if name not in self.data:
                self.logger.warning('{0} not in active CRIC endpoints, skipping'.format(name))
                continue

            plotlink = '<a href="plots/plots.php?endpoint={}">{}</a>'.format(name, name)
            html += '<tr><td>{}</td>'.format(plotlink)
            rse_attrs = self.rucio.list_rse_attributes(name)
            for attr in attr_list:
                if attr not in rse_attrs:
                    mark = '-'
                elif rse_attrs[attr]:
                    mark = 'y'
                else:
                    mark = 'n'
                html += '<td align="center">{}</td>'.format(mark)
            html += '</tr>\n'

        html += '</table><p>\n'
        html += '<p>\n'
        html += '<a href="index.html">Back to index</a><br>\n'
        html += 'This table was created using the RSE expression: <pre>%s</pre>\n' % expression
        html += '<p><b>"y"</b> means attribute is set to True, <b>"n"</b> means attribute is set to False, <b>"-"</b> means attribute is not set.\n'
        html += '<p>Generated at %s UTC on %s. This page is updated once per hour.</p>\n' % (str(datetime.utcnow()), socket.gethostname())
        html += '</body>\n</html>\n'
        with open('%s/%s.html' % (self.wwwdir, tablename), 'w') as f:
            f.write(html)

    def dumpjson(self):
        with open("%s/all_storage_data.json" % self.wwwdir, 'w') as f:
            json.dump(self.jsondata, f, sort_keys=True, indent=4)


def main():
    sm = StorageMonitor()

    sm.makeattributetable('type=DATADISK|type=TEST', 'DATADISK')
    # Fill html tables and make plots for each group of RSEs
    sm.maketable('tier=0&type=DATADISK|CERN-PROD_TZDISK=true', 'T0_DATADISK')
    sm.maketable('tier=1&type=DATADISK', 'T1_DATADISK')
    sm.maketable('tier=2&type=DATADISK', 'T2_DATADISK')
    sm.maketable('tier=3&type=DATADISK', 'T3_DATADISK')
    sm.maketable('tier=0&type=SCRATCHDISK', 'T0_SCRATCHDISK')
    sm.maketable('tier=1&type=SCRATCHDISK', 'T1_SCRATCHDISK')
    sm.maketable('tier=2&type=SCRATCHDISK', 'T2_SCRATCHDISK')
    sm.maketable('tier=3&type=SCRATCHDISK', 'T3_SCRATCHDISK')
    sm.maketable('type=CALIBDISK', 'CALIBDISK')
    sm.maketable('tier=0&type=MCTAPE', 'T0_MCTAPE', defaultlimit=10.0*1000**4)
    sm.maketable('tier=0&spacetoken=ATLASDATATAPE', 'T0_DATATAPE', defaultlimit=10*1000**4)
    sm.maketable('tier=1&type=MCTAPE', 'T1_MCTAPE', defaultlimit=10*1000**4)
    sm.maketable('tier=1&type=DATATAPE', 'T1_DATATAPE', defaultlimit=10*1000**4)
    sm.maketable('datapolicynucleus=true&type=DATADISK', 'NUCLEUS_DATADISK', plots=False)
    sm.maketable('datapolicynucleus=false&type=DATADISK', 'NON_NUCLEUS_DATADISK', plots=False)
    sm.maketable('type=GROUPTAPE', 'GROUPTAPE', defaultlimit=10*1000**4)
    sm.maketable('(tier=0|tier=1|tier=2)&(type=DATADISK|CERN-PROD_TZDISK=true)', 'ALL_PLEDGED_DATADISK', plots=False)
    sm.maketable('(tier=0|tier=1|tier=2)&(type=DATADISK|type=SCRATCHDISK|type=USERDISK|type=CALIBDISK|CERN-PROD_TZDISK=true|(type=GROUPDISK\spacetoken=ATLASDATADISK\IN2P3-CC_LOCALGROUPDISK-MW=true))', 'ALL_PLEDGED_DISK', plots=False)
    sm.maketable('(tier=0|tier=1|tier=2)&(spacetoken=ATLASDATATAPE|type=MCTAPE|spacetoken=ATLASGROUPTAPE)', 'ALL_PLEDGED_TAPE', plots=False)
    sm.maketable('(tier=0|tier=1|tier=2)&(type=DATADISK|type=SCRATCHDISK|type=USERDISK|type=CALIBDISK|type=MCTAPE|type=DATATAPE|spacetoken=ATLASGROUPTAPE|CERN-PROD_TZDISK=true)|(istape=true&site=CERN-PROD)|(type=GROUPDISK\spacetoken=ATLASDATADISK\IN2P3-CC_LOCALGROUPDISK-MW=true)', 'ALL_PLEDGED', plots=False)
    sm.maketable('type=LOCALGROUPDISK', 'LOCALGROUPDISK')
    sm.maketable('type=LOCALGROUPTAPE', 'LOCALGROUPTAPE', defaultlimit=10*1000**4)
    sm.maketable('type=TEST', 'TEST', defaultlimit=10*1000*4)
    sm.maketable('type=SPECIAL', 'SPECIAL', defaultlimit=10*1000*4)

    # dump json
    sm.dumpjson()
    sm.logger.info('Done')

if __name__ == '__main__':
    main()
