#!/bin/sh
RRDDIR=/var/www/html/ddmusr01/plots
BACKUPDIR=root://eosuser.cern.ch//eos/user/d/ddmusr01/rrdbackup/

cd $RRDDIR
ls *.rrd | while read f;
do
    r=`xrdcp -f "$f" "$BACKUPDIR/$f" 2>&1`
    code=$?
    if [ $code != 0 ] ; then
        echo "backup of $f failed!: $r"
    #else
        #echo "backed up $f"
    fi
done

