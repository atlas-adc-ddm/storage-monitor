# Storage monitor

This script generates several tables and plots, grouped by various combinations, eg T0, T1, T2, T3 and DATADISK, PRODDISK, SCRATCHDISK.

The tables are available at http://adc-ddm-mon.cern.ch/ddmusr01/

To fix strange values in the plots use the fix-rrd-fluctuations.py script. See the instructions inside to adjust the script according to the particular problem site, value, time etc.

## Installing

The setup on adc-ddm-mon is described in the [DDMOperationsScripts twiki](https://twiki.cern.ch/twiki/bin/view/AtlasComputing/DDMOperationsScripts#Storage_monitor_and_RRD_plots)

To develop and test locally:

  * Clone this gitlab project
  * Set up a python3 virtual environment: `python3 -mvenv storage-monitor-venv`
  * Activate the venv: `source storage-monitor-venv/bin/activate`
  * Install rrdtool: `pip install rrdtool`
  * Set up ATLAS_LOCAL_ROOT_BASE:
    * `export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase`
    * `source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q -3`
    * `lsetup rucio -q`

If you cannot use ATLAS_LOCAL_ROOT_BASE then install and configure rucio clients in the venv:
  * `pip install rucio-clients-atlas`
  * Set up rucio config: `cp storage-monitor-venv/etc/rucio.cfg.atlas.client.template storage-monitor-venv/etc/rucio.cfg` and edit as necessary

Create the hard-coded directory for the output `/var/www/html/ddmusr01/` and make it writable by your user, then copy the files in `web/` there:
  * `cp web/* /var/www/html/ddmusr01/`

## Running

The wrapper script `rse-usage.sh` has some hardcoded paths and variables for running on adc-ddm-mon. On other machines, follow the above set up and call the python script directly: `python3 rse-usage.py`
