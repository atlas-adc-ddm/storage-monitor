#!/usr/bin/python2
#
# Fixes bad values in rrdplots by dumping to xml, filling from the 
# last good value and restoring to the rrd file. A configurable factor is
# specified and if a number differs from the previous number by more than this
# factor then it is judged to be bad and replaced. The columns to look at can
# also be configured.
# Before running this script create .bak files eg 
#  for f in `ls /var/www/html/ddmusr01/plots/*.rrd`; do cp $f $f.bak; done
# These bak files can be restored in case something goes wrong. If everything
# is ok they can be removed after the rrds are fixed.

import os, subprocess, re

dir = '/var/www/html/ddmusr01/plots'
# The factor difference above which action will be taken
factor = 5
# Columns to look at: srmtotal, rucio, other, quotaother, darkdata, free, unlocked, limit, difference
#columnnumbers = [0,1,2,3,4,5,6]
columnnumbers = [4]

for rrd in os.listdir(dir):
    if rrd != 'PRAGUELCG2_DATADISK.rrd.bak' and not rrd.startswith('TOTAL'):
        continue
    if not re.search('.*rrd.bak$', rrd):
        continue
    endpoint = os.path.join(dir, rrd.replace('.rrd.bak', ''))
    print 'fixing %s.rrd' % endpoint
    p = subprocess.Popen(['rrdtool', 'dump', '%s.rrd.bak' % endpoint], stdout=subprocess.PIPE)
    (xml, err) = p.communicate()

    afterstart = False
    lastline = ''
    newxml = ''
    lastcolumns = []
    # Format of one row
    # <!-- 2017-03-17 11:00:00 CET / 1489744800 --> <row><v>5.6855188169e+15</v><v>5.5032368939e+15</v><v>0.0000000000e+00</v><v>0.0000000000e+00</v><v>-2.1844601427e+14</v><v>4.0072793721e+14</v><v>2.0713828645e+15</v><v>4.0004614478e+14</v><v>6.8179242943e+11</v></row>
    for line in xml.split('\n'):
        # Here a date can be specified to limit the changes
        columns = re.search(r'2015-01-.*<row><v>(.*)</v></row>', line)
        if not columns:
            newxml += line + '\n'
            lastline = line
            continue
        
        if re.search('NaN', line) and not afterstart:
            newxml += line + '\n'
            lastline = line
            continue

        # We are in a data row after the start
        afterstart = True
        lineprefix, linedata = line.split('<row>')

        columns = columns.group(1).split('</v><v>')
        columns = [0 if c == 'NaN' else float(c) for c in columns]
        if not lastcolumns: # first line of data
            lastcolumns = columns
            newxml += line + '\n'
            lastline = line
            continue

        replaced = False
        for (prev, curr, seq) in zip(lastcolumns, columns, range(len(columns))):
            if seq not in columnnumbers:
                continue
            if (prev == 0 and curr != 0) or (prev != 0 and curr == 0) or (prev < 0 and curr > 0) or ((prev != 0 and curr != 0) and (abs(prev) / abs(curr) > factor or abs(curr) / abs(prev) > factor)):
                print 'fix line %s' % line
                newline = lineprefix + '<row>' + lastline.split('<row>')[1]
                print 'replacement %s' % newline
                # replace line with previous line
                newxml += newline + '\n'
                replaced = True
                line = newline
                break
        
        if not replaced:
            newxml += line + '\n'
            lastcolumns = columns
        lastline = line

    with open('%s.xml' % endpoint, 'w') as f:
        f.write(newxml)

    # After testing uncomment this line to write the rrd file back
    # p = subprocess.Popen(['rrdtool', 'restore', '-f', '%s.xml' % endpoint, '%s.rrd' % endpoint])

